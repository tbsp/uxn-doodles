#/bin/python

#
# Generate a sine table comprised of 256 16-bit entries spanning +-32768
#

SHORT = False

import math

def chunks(lst, n):
    """Yield successive n-sized chunks from lst."""
    for i in range(0, len(lst), n):
        yield lst[i:i + n]


if SHORT:
    values = []
    angle = 0.0
    for i in range(256):
        values.append(65536 / 2 * math.sin(angle))
        angle += (2 * math.pi) / 256.0

    for chunk in chunks(values, 16):
        print('[ {} ]'.format(' '.join(['{:04x}'.format(0xffff & int(item)) for item in chunk])))
else:
    values = []
    angle = 0.0
    for i in range(256):
        values.append(0x50 / 2 * math.sin(angle))
        angle += (2 * math.pi) / 256.0

    for chunk in chunks(values, 16):
        print('[ {} ]'.format(' '.join(['{:02x}'.format(0xff & int(item)) for item in chunk])))

